Le projet e-NDP (Anr 2020) engage l’examen des registres capitulaires, de la documentation et des livres de Notre-Dame de Paris au Moyen Âge, pour étudier la société, l’économie, le bâti du cloître et de ses dépendances. Porté par le LaMOP, le projet a pour partenaires les Archives nationales, la Bibliothèque nationale de France (Département des Manuscrits, Bibliothèque de l’Arsenal), l’École nationale des Chartes et la Bibliothèque Mazarine.

Le projet E-Ndp est coordonné par Julie Claustre (MC/université Paris I Panthéon-Sorbonne/LaMOP) et Darwin Smith (DR/CNRS/LaMOP).

L'ensemble de ces données est distribuée sous licence Creative Commons attribution, partage dans les mêmes conditions (https://creativecommons.org/licenses/by-sa/3.0/deed.fr)

Contact : e-npd@lamop.fr

Pour citer ces données
Registre LL 108/A, Archives Nationales - Laboratoire LaMOP, Transcription by Mathilde Treglia and Anne Massoni (Université de Limoges), Folios 1 recto-27 recto, in Projet E-NDP

Notes :
09/02/2021:
Version 1 : Première version
